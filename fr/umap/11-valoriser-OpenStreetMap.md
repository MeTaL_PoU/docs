# Je valorise les données OpenStreetMap avec Overpass et uMap

Un bon moyen de valoriser des données OpenStreetMap consiste à intégrer le résultat d'une requête Overpass dans une carte umap. Plusieurs approches sont possibles.

## Importer des données statiques

Dans umap l'opération **Importer des données** permet de sélectionner un fichier dont le contenu est affiché sur la carte. Plusieurs formats de fichiers dont les formats GeoJSON et OSM sont acceptés. Le contenu du fichier est alors stocké sur le serveur umap.

C'est par exemple le cas de cette [carte du patrimoine de Nantes](http://umap.openstreetmap.fr/fr/map/patrimoine-de-nantes_22573), qui affiche dans deux couches distinctes les monuments classés et les monuments incrits à l'inventaire national.

<iframe width="80%" height="200px" style="margin-left:80px; border:solid black 1px;" frameBorder="0" src="http://umap.openstreetmap.fr/fr/map/patrimoine-de-nantes_22573?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=false&datalayersControl=true&onLoadPanel=none&captionBar=false"></iframe><p style="margin-left:80px;"><a href="http://umap.openstreetmap.fr/fr/map/patrimoine-de-nantes_22573">Voir en plein écran</a></p>

Il est judicieux d'importer un fichier dans un calque séparé. Cela permet notamment de configurer les popups pour qu'elles affichent les données textuelles des données importées. Cela se fait en définissant le **Gabarit du contenu de la popup** dans le menu **Popup Options**. Cette possibilité est utilisée pour le calque Patrimoine de la carte ci-dessus.

## Utiliser des données distantes

Au lieu d'importer des données sur le serveur umap, il est possible de stocker ces mêmes données sur un autre serveur et de configurer un calque umap pour qu'il utilise ces données. Cela se définit dans le menu **Données distantes** du calque. Il peut être nécessaire de cocher l'option **Avec proxy** selon la configuration du serveur.

L'intérêt de cette approche est que l'on peut définir les niveaux de zoom auxquels ces données sont affichées. On configure pour cela les paramètres **A partir du zoom** et **Jusqu'au zoom** dans le menu **Données distantes** des calques. On peut alors créer une carte qui, en fonction de l'échelle de la carte, affiche différentes données et/ou les mêmes données de différentes manières.

C'est ce que fait cette carte des parkings à vélo de Nantes, qui affiche les mêmes données sous forme de *heatmap*((carte de chaleur : les données sont affichées sous forme de zones de couleur montrant la densité des données, ou d'une variable de ces données)) jusqu'au zoom 16, et chaque parking individuellement à partir du zoom 17. Notez que la carte de chaleur utilise le tag **capacity**, et montre bien la capacité de stationnement et non le nombre de parkings.

<iframe width="80%" height="200px" style="margin-left:80px; border:solid black 1px;" frameBorder="1" src="https://umap.openstreetmap.fr/fr/map/nantes-a-velo_21209?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=false&datalayersControl=false&onLoadPanel=none&captionBar=false"></iframe><p style="margin-left:80px;"><a href="https://umap.openstreetmap.fr/fr/map/nantes-a-velo_21209">Voir en plein écran</a></p>

## Afficher le résultat d'une requête Overpass

Le calque **Stations Bicloo** de la [carte des parkings à vélo](https://umap.openstreetmap.fr/fr/map/nantes-a-velo_21209) montre, à partir du zoom 17, les stations Bicloo de Nantes. Celles-ci proviennent directement d'une requête Overpass. L'intérêt est que la carte restera toujours à jour des données OpenStreetMap, l'inconvénient est qu'il faut attendre que la requête s'exécute pour voir les données.

Voici les étapes pour définir un tel calque :

 1.  créer, simplifier et tester la requête avec **Overpass Turbo**
 2.  afficher l'URL de la requête : `Exporter > Requête > Overpass QL (compact)`
 3.  copier l'adresse du lien affiché, qui commence par `[out:xml]`
 4.  dans **umap** créer un nouveau calque et coller cette adresse dans le champ **URL** du menu **Données distantes**
 5.  sélectionner le format **osm**

Notez l'export de la requête ajoute l'URL du serveur Overpass, et encode la requête proprement dite pour qu'elle puisse être transmise par le protocole HTTP. Ainsi la requête :

<pre><code>[out:json][timeout:25]; node["amenity"="bicycle_rental"](47.17,-1.62,47.27,-1.48); out body qt;</code></pre>

est transformée par la fonction Export de Overpass Turbo en :

<pre><code>http://overpass-api.de/api/interpreter?data=%5Bout%3Ajson%5D%5Btimeout%3A25%5D%3Bnode%5B%22amenity%22%3D%22bicycle%5Frental%22%5D%2847%2E17%2C%2D1%2E62%2C47%2E27%2C%2D1%2E48%29%3Bout%20body%20qt%3B%0A</code></pre>

Un petit gain de performance peut être obtenu en évitant la clause **in**, qui doit d'abord trouver le lieu par géocodage puis construire le polygone correspondant. Il est préférable d'effectuer une sélection géographique basée sur un rectangle (bbox) : l'export de la requête avec Overpass Turbo définit ce rectangle à partir de la carte visible.

Selon le volume et la densité des données attendues, optez pour un affichage manuel du calque (décochez l'option Afficher au chargement) ou définissez le niveau de zoom auxquel afficher ces données.

## Utiliser une requête Overpass dynamique

![](images/umaprequetedynamique.jpg)

Si le temps d'exécution de la requête Overpass est trop long parce que le volume de donneés est important, essayez d'utiliser une requête dynamique qui prendra en compte la *bounding box* visible à l'écran. Pour cela rédigez une requête Overpass qui utilise le filtre géographique `{{bbox}}` - exemple avec les aires de jeux :

    [out:xml];
    node[leisure=playground]({{bbox}});
    out body;

Remplacez `{{bbox}}` par `{south},{west},{north},{east}`, ajoutez l'URL du serveur Overpass et assemblez le tout sur une ligne. Vous obtenez la requête HTTP suivante :

    http://overpass-api.de/api/interpreter?data=[out:xml];node[leisure=playground]({south},{west},{north},{east});out body;

Dans uMap, crééez un calque et copiez cette requête dans le champ URL du menu **Données distantes**. Choisissez le format **osm** qui correspond à la clause Overpass ''[out:xml]''. Pensez à cocher l'option **Dynamique** qui signale que les variables {south}, {west} etc. doivent être remplacés par les latitudes et longitudes de la carte. Le tour est joué !

<p class="alert alert-warning">
N'utilisez pas la variable {bbox} car elle sera remplacée par des coordonnées dont l'ordre (W,S,N,E) n'est pas celui attendu par Overpass (S,W,N,E) !
</p>
<p class="alert alert-info">
Si l'affichage des données reste trop lent, précisez un niveau de zoom dans le champ <b>A partir du zoom</b> : le calque ne sera affiché - et la requête exécutée - qu'à partir de ce niveau de zoom. Plus celui-ci est élevé plus la bounding box de la requête et le volume de données récupéeés sont réduites.
</p>