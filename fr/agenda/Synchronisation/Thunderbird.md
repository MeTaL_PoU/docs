# Synchronisation avec Thunderbird
[<span class="glyphicon glyphicon-arrow-left"></span> Retour à l'accueil](../README.md)

Thunderbird est un client de messagerie électronique développé par Mozilla, l'organisation derrière le navigateur Firefox. Il s'agit d'une alternative à Outlook. Thunderbird permet outre les messages électroniques, de gérer ses contacts, ses agendas et ses tâches grâce à l'extension Lightning, désormais intégrée au logiciel. L'extension [Cardbook](https://addons.mozilla.org/fr/thunderbird/addon/cardbook/) nous sera également nécessaire afin de synchroniser nos contacts.

## Installation de Thunderbird

Si Thunderbird est déjà installé et configuré sur votre machine, vous pouvez sauter cette partie.

Thunderbird s'installe à partir de [son site officiel](https://www.mozilla.org/fr/thunderbird/) pour Windows et Mac, tandis que les utilisateurs sous Linux peuvent utiliser le gestionnaire de packets de leur distribution (le logiciel se nomme Icedove sous Debian).

Une fois l'application lancée, vous pouvez vous configurer un compte mail, mais également sauter cette étape. Pour accéder au module d'agenda, cliquez sur l'icône **Aller à l'onglet d'Agenda** en haut à droite de l'interface ou rendez-vous dans le menu **Événements et Tâches > Agenda**.

## Synchronisation des agendas

*Thunderbird ne supporte pas la découverte d'agendas sur Framagenda, ce qui signifie que vous devez ajouter chaque agenda manuellement.*

Pour synchroniser un agenda de Framagenda avec Thunderbird, récupérez son lien sur Framagenda. Il faut cliquer sur les trois petits points puis dans le menu, cliquer sur **Lien** et récupérez le lien affiché dans le champ de texte.

![Lien CalDAV pour un Agenda](../images/agenda-6.png)

Dans Thundebird, cliquez sur **Créer un nouvel agenda** sur la page d'accueil ou bien cliquez-droit sur la zone de la liste des agendas.

![Assistant Thunderbird](../images/Thunderbird-1.png)

Lorsque l'assistant est lancé, choisissez agenda **Sur le réseau** puis à l'étape suivante, choisissez le format CalDAV. Entrez comme emplacement l'adresse copiée précédemment et vérifiez que la case **Prise en charge du mode hors connexion** est cochée. En cliquant sur **Suivant**, vous pouvez choisir le nom du calendrier et sa couleur. Une fois l'assistant terminé, une boite de dialogue vous invite à entrer vos informations de connexion à Framagenda.

Note : Si vous utilisez [l'authentification en deux étapes](../Inscription-Connexion.md#facultatif-utiliser-lauthentification-en-deux-tapes-2fa), vous devez créer [un mot de passe d'application](../Inscription-Connexion.md#utiliser-les-mots-de-passe-dapplication).

Une fois l'agenda configuré, ses événements sont visibles dans l'interface.

![Agenda Thunderbird](../images/Thunderbird-2.png)

Les tâches associées au calendrier sont également synchronisées automatiquement.

<p class="alert alert-warning">
REMARQUE : Si vous avez des difficultés de synchronisation de vos agendas avec Thunderbird 60, vous devez changer la valeur du paramètre "network.cookie.same-site.enabled" dans l'éditeur de configuration de Thunderbird (menu édition, préférences, onglet "avancé") de TRUE à FALSE. Pour plus d'informations, voir <a href="https://help.nextcloud.com/t/thunderbird-60-problems-with-address-and-calendar-sync/35773">la discussion</a> (en anglais) sur le forum Nextcloud.
</p>

## Synchronisation des contacts

Thunderbird nécessite le module complémentaire [Cardbook](https://addons.mozilla.org/fr/thunderbird/addon/cardbook/) afin de synchroniser les contacts de Framagenda. Une fois le module installé, vous avez une nouvelle icône en haut à droite de l'interface vous permettant d'accéder à la synchronisation des contacts.

![Module des contacts](../images/Thunderbird-3.png)

Cliquez sur **Ajouter un carnet d'adresses**, puis sélectionnez **Carnet d'adresses distant**. À l'étape suivante, le type de carnet d'adresses à choisir est CardDAV.

L'URL à renseigner est celle trouvée sur le carnet d'adresses dans l'application Contacts de Framagenda, c'est-à-dire celle dans les paramètres en bas à gauche, en cliquant sur le lien de votre carnet d'adresses.

![Lien CardDAV](../images/contacts-4.png)

Enfin, vous devez renseigner vos informations de connexion de Framagenda avant de cliquer sur **Valider**.

Note : Si vous utilisez [l'authentification en deux étapes](../Inscription-Connexion.md#facultatif-utiliser-lauthentification-en-deux-tapes-2fa), vous devez créer [un mot de passe d'application](../Inscription-Connexion.md#utiliser-les-mots-de-passe-dapplication).

![Module de sync](../images/Thunderbird-4.png)

Si la connexion a été établie, vous pouvez cliquer sur **Suivant** et choisir la couleur et le nom du carnet d'adresses dans Thunderbird. Une fois l'assistant achevé, les contacts se synchronisent avec Framagenda. Vous pouvez gérer vos contacts en cliquant sur **Ajouter un contact**, **Éditer le contact** ou encore **Supprimer le contact**.

<p class="alert alert-warning">
REMARQUE : Si vous avez des difficultés de synchronisation de vos contacts avec Thunderbird 60, vous devez changer la valeur du paramètre "network.cookie.same-site.enabled" dans l'éditeur de configuration de Thunderbird (menu édition, préférences, onglet "avancé") de TRUE à FALSE. Pour plus d'informations, voir <a href="https://help.nextcloud.com/t/thunderbird-60-problems-with-address-and-calendar-sync/35773">la discussion</a> (en anglais) sur le forum Nextcloud.
</p>
