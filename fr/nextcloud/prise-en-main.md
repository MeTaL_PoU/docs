# Prise en main

## Partage par lien

Pour partager un fichier ou un dossier vous devez :

  * cliquer sur l'icône de partage <i class="fa fa-share-alt"></i> (**si vous ne voyez pas l'icône pensez à désactiver votre bloqueur de pub**)
  * cocher **Activer** sur la ligne **Partager par lien public**
  * cliquer sur l'icône <i class="fa fa-ellipsis-h"></i>
  * cliquer sur **Copier l'adresse URL** (le lien est dans votre presse-papier : vous pouvez donc le coller dans un mail ou autres, pour le partager)

![image de partage framadrive](images/drive-partage.png)

En cliquant sur <i class="fa fa-ellipsis-h"></i> vous avez la possibilité de paramétrer les conditions de partage.
