Démarrer sur diaspora\*
=======================

3ème partie - Les aspects
-------------------------

Réfléchissez à votre vie et aux personnes que vous connaissez. Chaque
personne fait partie d'un ou plusieurs aspects de votre vie. Ils peuvent
être un membre de votre famille, un(e) ami(e) proche, un(e) collègue de
travail ou quelqu'un avec qui vous faites du sport ou de la musique ou
avec qui vous partagez un centre d'intérêt. Ou ils peuvent faire partie
de plusieurs de ces aspects de votre vie.

diaspora\* fonctionne exactement de la même manière. Vous pouvez placer
vos contacts dans des "aspects" de votre graine diaspora\* selon les
aspects de votre vie dont ils font partie. Ainsi, vous contrôlez
lesquels, parmi vos contacts, voient chacune des publications que vous
effectuez sur diaspora\* en publiant sur un ou plusieurs de vos aspects.
Nous expliquerons plus en détail ce que cela signifie dans les trois
sections suivantes.

#### Mes aspects

Ensuite, jetons un œil au menu **Mes aspects** dans la colonne de gauche.
Revenez à votre flux et cliquez sur **Mes aspects**. La liste de vos aspects
va s'afficher et vous verrez un flux ne contenant que les articles
publiés par les membres de l'aspect sur lequel vous cliquerez dans la
liste.

-   Maintenant, tous vos aspects devraient être sélectionnés, avec une
    petite coche à côté de chacun d'eux.
-   Vous pouvez cliquer sur les aspects dans le menu pour les
    (dé)sélectionner.
-   Vous pouvez choisir un seul aspect ou une combinaison de plusieurs
    d'entre eux.
-   Si tous les aspects sont sélectionnés et que vous voulez n'en voir
    qu'un, cliquez sur Désélectionner tout et sélectionnez celui que
    vous voulez voir.

Essayez de jouer avec quelques instants.

Ce système de sélection a deux usages :

1.  Cela filtre le contenu du flux. Vous pouvez ne vouloir lire que les
    articles publiés par les personnes de votre aspect `Famille`. Ou vous
    pourriez vouloir lire tout ce que vos contacts ont posté *sauf* les
    membres de votre famille.
2.  Si vous publiez une activité depuis la page des **Aspects**, les aspects
    sélectionnés dans le menu de gauche le seront automatiquement lors
    de la publication. Par exemple, si les aspects `Amis`, `Famille` et
    `Travail` sont sélectionnés dans la liste, quand vous cliquerez dans
    la fenêtre de publication, vous verrez que le bouton de sélection
    d'Aspects indiquera "3 aspects".

![Aspect
list](images/aspect-1.png)

#### Ajouter un aspect

Choisissez un nom pour cet aspect. Il peut exprimer la relation qui
existe entre ces personnes, par exemple "Écriture créative", "Football"
ou "Activistes" mais vous pouvez les nommer comme bon vous semble.
Personne d'autre que vous ne verra jamais les noms de vos aspects, pas
plus que vos contacts ne sauront dans quel(s) aspect(s) ils se trouvent.
Pour cet exercice, je vous suggère de créer un aspect nommé "Diaspora"
dans lequel vous placerez les personnes que vous rencontrez ici.

**Note** : si vous utilisez la version mobile, vous devez passer à la version bureau en cliquant sur « Activer/désactiver la version mobile » dans le menu latéral, puisqu'il n'est, pour le moment, pas possible d'ajouter un aspect avec la version mobile.

Quand vous avez terminé, cliquez sur le bouton Créer. L'aspect sera
alors créé et sélectionné dans le menu.

![Add
aspect](images/aspect-2.png)

1.  Une fois que l'aspect est créé, vous serez conduit(e) à une liste de
    contacts associés à cet aspect (qui sera vide). Vous trouverez un
    lien pour ajouter un ou plusieurs contact(s) à cet aspect.
2.  Une fois fini, cliquez sur le bouton « Créer ». L'aspect sera créé et sélectionné dans le menu.

Une fois que l'aspect est créé, vous serez redirigé sur votre page « Contacts » où vous verrez la liste des membres de cet apsect (vide pour le moment). Même si cet aspect est vude, vous verrez une liste des autres contacts, avec un bouton à côté de chacun d'eux pour les ajouter facilement à l'aspect.

Vous avez la possibilité de changer l'aspect par défaut de vos publication (paramétré sur **Tous les aspects**) en allant dans [vos paramètres](https://framasphere.org/user/edit), puis en passant le paramètre **Aspects sélectionnés par défaut pour la publication** à **Public** (par exemple) puis en cliquant sur **Modifier**.

### Page des contacts

Nous avons mentionné votre page des contacts dans la [Partie
2](2-interface.html). Pour vous
rendre sur votre page des contacts, cliquez sur la flèche tout à droite
de la barre d'en-tête noire et sélectionnez Contacts dans la liste
déroulante.

La page des contacts indique vos aspects dans la colonne de gauche et
vos contacts se trouvant dans ces aspects dans la colonne de droite.

Si vous affichez les contacts de tous les aspects (cliquez sur Mes
contacts dans la colonne de gauche), chaque contact disposera d'un
bouton vert sur sa droite indiquant les aspects dans lesquels il a été
placé. Si un contact se trouve dans plus d'un aspect, le bouton
indiquera "Dans *n* aspects". Cliquez sur le bouton pour voir quels sont
ces aspects.

Vous pouvez cliquez sur ce bouton pour modifier les aspects dans
lesquels se trouve cette personne en (dé)sélectionnant les aspects dans
la liste déroulante. Si vous souhaitez retirer une personne de tous vos
aspects, dé-sélectionnez tous les aspects et le bouton deviendra gris.
Vous ne partagerez alors plus avec elle.

Si vous visualisez les contacts d'un seul aspect, vous verrez une croix
à droite de chacun des contacts affichés. Cliquer dessus retirera cette
personne de l'aspect.

Vous pouvez également créer de nouveaux aspects depuis votre page des
contacts.

#### Partage uniquement avec moi

Ceci vous montre une liste de personnes qui partagent avec vous mais
avec lesquelles vous ne partagez pas - vos "abonnés".

Maintenant que vous avez compris les aspects, faisons quelques
connexions.

[2ème partie -
L'interface](2-interface.html) |
[4ème partie - Trouver des gens et se connecter avec
eux](4-echange.html)

#### Ressources utiles

-   [Base de code](http://github.com/diaspora/diaspora/)
-   [Documentation](https://wiki.diasporafoundation.org/)
-   [Trouver et signaler des
    bugs](http://github.com/diaspora/diaspora/issues)
-   [IRC - Général](http://webchat.freenode.net/?channels=diaspora)
-   [IRC -
    Développement](http://webchat.freenode.net/?channels=diaspora-dev)
-   [Liste - Général](http://groups.google.com/group/diaspora-discuss)
-   [Liste - Développement](http://groups.google.com/group/diaspora-dev)

[![Licence Creative
Commons](images/cc_by.png)](http://creativecommons.org/licenses/by/3.0/)
[diasporafoundation.org](https://diasporafoundation.org/) est protégé
sous licence [Licence publique Creative Commons Attribution 3.0 non
transposée](http://creativecommons.org/licenses/by/3.0/)
