# Framabin

[Framabin](https://framabin.org) est un service en ligne libre et
minimaliste qui permet de partager des textes de manière confidentielle et sécurisée.

  1. Collez le texte à transmettre.
  2. Si besoin, définissez sa durée de conservation en ligne, ajoutez
     un espace de discussion ou activez la coloration du code.
  3. Partagez ensuite avec vos correspondants le lien qui vous est donné.

Vos données (commentaires inclus) sont **chiffrées dans le navigateur web**
en utilisant l’[algorithme AES 256 bits](http://fr.wikipedia.org/wiki/Advanced_Encryption_Standard).
Elles sont ensuite transmises et stockées sur nos serveurs
sans qu’il nous soit possible de les déchiffrer.
**Vous seuls possedez la clé** utilisée pour chiffrer et déchiffrer les données.

Le service repose sur le logiciel libre [PrivateBin](https://privatebin.info).

---

## Tutoriel vidéo

<div class="text-center">
  <iframe width="560" height="315" src="https://framatube.org/videos/embed/5add5769-407c-4c28-9a2c-a64c9a092b9f" frameborder="0" allowfullscreen></iframe>
</div>

Vidéo réalisée par [arpinux](http://arpinux.org/), artisan paysagiste de la distribution GNU/Linux pour débutant [HandyLinux](https://handylinux.org/)
