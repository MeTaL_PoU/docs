# Paramètres du canal


_____

Les préférences de notification, l'en-tête du canal et le sujet du canal sont personnalisables pour chaque canal.

## Préférences de notification des canaux

Les préférences de notification peuvent être modifiées pour chaque canal dont vous êtes membre.

#### Envoyer les notifications 

Par défaut, les préférences des notifications de bureau définies dans *Paramètres du compte* sont utilisées pour tous les canaux. Pour personnaliser les notifications de bureau pour chaque canal, cliquez sur le nom du canal en haut du panneau central pour accéder au menu déroulant, puis cliquez sur **Préférences de notifications > Envoyer des notifications de bureau**.


#### Marquer les canaux non lus

Personnalisez les événements qui affichent le nom des canaux en gras dans le menu déroulant puis dans **Préférences de notification > Marquer les canaux non lus**.

## En-tête du canal 

L'en-tête du canal est le texte qui apparaît à côté du nom du canal en haut de l'écran. Il peut être utilisé pour résumer le sujet du canal, ou pour fournir un lien vers les documents fréquemment consultés.  N'importe quel membre du canal peut modifier l'en-tête en cliquant sur le nom du canal en haut du panneau central pour accéder au menu déroulant, puis en cliquant sur **Régler l'en-tête du canal**.

## But du canal

Ce texte apparaît dans la liste des canaux dans le menu *Plus* et aide les utilisateurs à décider de rejoindre le canal ou non. N'importe quel membre du canal peut modifier le but du canal en cliquant sur le nom du canal en haut du panneau central pour accéder au menu déroulant, puis en cliquant sur **Régler le but du canal**.
