# Créer une galerie photos sur Framapic

<a href="https://framapic.org"><b class="violet">Frama</b><b class="vert">pic</b></a>, (à l'aide du <a href="https://www.alsacreations.com/article/lire/1402-web-storage-localstorage-sessionstorage.html">localstorage</a>), peut se souvenir des images que vous avez versées sur nos serveurs.

Il ne manquait plus qu'un outil permettant de piocher dans vos images pour choisir ce que vous désirez mettre dans votre galerie&hellip; C'est chose faite&nbsp;!

<h2>La preuve en images</h2>

Pouhiou veut rassembler les images des deux derniers Framabooks (<a href="https://framabook.org/la-nef-des-loups/">La Nef des Loups</a>, et <a href="https://framabook.org/grise-bouille-tome-ii/">Grise Bouille Tome 2</a>) et de leurs auteurs dans une galerie. Il sait qu'il a déjà envoyé la couverture de Grise Bouille et la photo de son auteur, Gee, sur <a href="https://framapic.org"><b class="violet">Frama</b><b class="vert">pic</b></a>. Il commence donc par ajouter les fichiers de La Nef des loups&nbsp;: la couverture du livre et la photo de Yann Kervran, son auteur.

![Envoi d'images sur Framapic](images/pic-envoi-images.png)

Alors certes, Pouhiou voit qu'il a un lien vers la galerie de ces deux images (cf. l'encadré rouge), mais les autres photos se trouvent déjà dans <a href="https://framapic.org"><b class="violet">Frama</b><b class="vert">pic</b></a>&hellip; Et puis Pouhiou est une tête de linotte, il oublie de copier ce lien et ferme cet onglet pour aller voir une vidéo de tricot.

Quelques heures plus tard (oui&nbsp;: le tricot, c'est long), Pouhiou revient sur <a href="https://framapic.org"><b class="violet">Frama</b><b class="vert">pic</b></a> et décide de construire sa galerie. Pour cela, il clique sur "Mes images".

![mes images sur Framapic](images/pic-mes-images.png)

Là, il se trouve devant un tableau récapitulant les images qu'il a déjà ajoutées à <a href="https://framapic.org"><b class="violet">Frama</b><b class="vert">pic</b></a>. Il lui suffit de cocher les quatre qui l'intéressent et de copier le lien de la galerie, en haut (d'ailleurs, le petit bouton à droite du lien de la galerie copie automatiquement ce long lien dans son presse-papier&hellip; pratique&nbsp;!)

![ajout d'images dans une galerie Framapic](images/pic-animation-galerie.gif)

Une fois le lien copié, il lui suffit de le coller dans la barre d'adresse de son navigateur pour le voir, ou dans un email, par exemple, s'il veut le partager. Et <a href="https://framapic.org/gallery#PaLb7QnHHkDF/HfhhltrLk6UW.png,CQuxwcRFzDiN/c9OHk27wsGEc.png,1NBkcbCgeBO5/0AKZ63dASdf9.png,x5IlRxx45pMu/FoXHKHd2hg4t.jpg">le résultat est là</a>&nbsp;!

![résultat d'une galerie Framapic](images/pic-resultat-galerie.png)

Voilà, désormais, créer une galerie d'images qui respecte les données de chacun·e, c'est simple comme quelques clics&nbsp;!

<h3>Pour aller plus loin&nbsp;:</h3>
<ul>
 	<li>Essayer <a href="https://framapic.org"><b class="violet">Frama</b><b class="vert">pic</b></a> ou <a href="https://lut.im">Lut.im</a>.</li>
 	<li>Participer <a href="https://framagit.org/luc/lutim">au code de Lutim</a></li>
 	<li>Faire un don à Framasky (rappelons qu’il l’a codé sur son temps perso ^^)&nbsp;: <a href="https://liberapay.com/sky/">Liberapay</a> / <a href="https://tipeee.com/fiat-tux">Tipeee</a></li>
</ul>
