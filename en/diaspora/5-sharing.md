Getting started in diaspora\*
=============================

Part 5 – Start sharing!
-----------------------

Now that you have some contacts, it’s time to start sharing content with
them.

You can communicate via diaspora\* either by sharing a status message
with a group of followers, or even to the whole of diaspora\*; or by
sending a private message to one or more mutual contacts. In this part
we will focus on using the publisher and content stream to post and
comment on status messages. We’ll look at sending a private message
(which we call a “conversation”) in [Part
6](6-conversations.html).

### The publisher

Sharing content in diaspora\* is done via the publisher, which is
located at the top of the middle column on most pages in the diaspora\*
interface. You have probably already clicked it, and if you did you
probably weren’t able to resist posting something! Posting a status
message is as simple as can be. A bit later in this tutorial we’ll look
at some of the more complex things you can do with the publisher, but
let’s start with an introduction.

The publisher may look like a dull, narrow box but don’t let its
appearance deceive you: beneath its minimalist appearance lies a
treasure trove of features, which you can trigger by clicking inside the
box.

![Publisher](images/partage-1.png)

Once the publisher box has “come alive,” you’ll see a range of buttons
and icons. Before we look at them in detail, let’s ignore them and post
a simple status message available only to your followers and mutual
contacts.

To do this, all you need to do is to type your message – whatever you
want to say to them, perhaps “This is my first post in Diaspora” – and
press the Share button. And you’ve shared a message with your contacts!

OK, so what are those buttons all about?

#### Text formatting

Above the publisher window you’ll see two tabs and a row of buttons. The
tabs display your raw message text and a preview of how your finished
message will look. The buttons are there to help you format your text
and add other things to your message:

-   Bold
-   Italic
-   Heading
-   Link
-   Image
-   Bulleted list
-   Numbered list
-   Code
-   Quoted text

#### Aspect selector button

This button is how you select who will be able to read your post. It is
set to “All aspects” by default: that is, everyone you have added to one
of your aspects will be able to read the message, but no one else will
be. In the simple message you just sent, because you wanted it visible
just to your followers and the default is “All aspects,” there was no
need to change any settings before sharing your message.

With this button you can select any individual aspect to share with, or
any combination of your aspects, by clicking on aspects in the list to
select or unselect them. In this way you have complete control over who
gets to read your messages, as we discussed in [Part
3](3-aspects.html). If, on the other hand, you want to
announce something to the whole world, select “Public,” and there will
be no restrictions on who can read your message.

#### Adding a poll

![Aspects select](images/partage-2.png)

The icon at the bottom right of the publisher that looks like a graph
enables you to add a poll to your post. Click it and a box will open
into which you can enter a question you want to poll people on, and two
or more answers for them to give. Each time you enter an answer in the
last field, another will appear below, so you can give as many answer
options as you like.

#### Photo sharing

At the right-hand end of the text field is a camera icon that lets you
upload photos to your message. You can either click on it and select
some pictures from your computer, or drag the images straight from a
folder to the button.

#### Locator

Next to the camera is a “pin” icon which activates the locator feature.
This enables you to add your location to posts. Click it and it will ask
you whether you’re happy for it to determine your location using
OpenStreetMap, and if you allow this it will add your location to your
post as a footnote.

You can edit this location by clicking on the text once it appears under
the publisher window.

The location provided may be very exact, so you might want to edit out
the specific address information and just give the town or region you’re
in. It’s up to you!

#### Connected services

Based on the connections you have made with your accounts on other
social networks (Facebook, Twitter and Tumblr), there may be icons for
these services under the publisher. Highlighting these posts your
message to those services. We’ll cover this in the next section, below.

The icon next to your connected services looks like a spanner. Clicking
this will let you configure your connections with other social networks
and services.

We’ll look at how to connect to other services in [Part
7](7-finishing.html).

That is all there is to know about the buttons and icons on the
publisher. The real magic happens within the publisher itself. As well
as plain text, you can format your text, add \#tags, @mentions and
links, and embed photos, videos and audio. Wow!

### Posting features

#### \#tags

Place a \# (hash) symbol in front of a word and it becomes a \#tag.
What’s the use of this? Well, \#tags are a great way of finding content
that interests you, and of bringing content to the attention of people
who are likely to be interested in it. You can search for tags by typing
them in the search field in the header. You can also follow tags, so
that any posts containing those tags appear in your stream – in fact, if
you filled out the “Describe yourself in 5 words” part of your profile
page, you’re already following those tags!

Say you like cooking and you post a message with “\#cooking” in it.
Anyone who follows the \#cooking tag will now see your post appear in
their streams. Of course for any follower of \#cooking to see your post,
you’ll have had to make it a public post; if you made it only to one or
more aspects, only people in those aspects will be able to read it. You
can also search for “\#cooking” in the search bar, and you’ll be
presented with a list of all posts tagged with “\#cooking.” That
includes your own posts, posts by friends and public posts by other
diasporans. You can see how this can be a fantastic tool for finding and
sharing content with people who share your interests.

If your post might potentially cause offence or get someone into trouble
for viewing it at work, please add the \#nsfw (“not safe for work”) tag
so that it will be hidden on people’s screens unless they choose to view
it. We’ll cover this in [Part 7](7-finishing.html).

#### @mentions

Did you know you can grab someone’s attention by “mentioning” them?
Simply type a @ symbol followed by the name of one of your contacts. As
you start typing their name (their screen name, not their user name) it
will automatically be completed by diaspora\*. Hit enter or click the
name in the auto-completer and you’ll notice that it has changed to
their full name, and the @ symbol has disappeared. Don’t worry, though;
this will become an @mention once you have posted the message. The
person you are mentioning will receive a notification of the mention on
their notification page, and by email if they have asked to receive
email notification of mentions.

![Mentions](images/partage-3.png)

Likewise other people can @mention you, and you’ll receive a
notification in the header bar (and by email if you have set that
option).

Note that you can only @mention people in your aspects; and only in
posts, not in comments.

#### Text formatting

There’s a lot you can do to make your text more varied: bold, italics,
large headings and so on. Most of this formatting can be done using the
buttons above the publisher – you don’t need to worry about that messy
code.

To add *italic*, **bold**, or inline code, highlight the text you want
to format and click the relevant button from the row above the publisher
window. The Markdown code for this formatting will be placed around this
portion of text.

To create a heading, list or quotation, place your cursor on the line
you want to format, and press the relevant button.

#### Links

Want to include a link in your post? Just click the link button and
paste the link URL into the pop-up which appears. This will enter the
URL into your post, wrapped in Markdown link code. Type the text you
want to appear in your message over the highlighted text in square
brackets.

If you want to include a bare link into your message, simply paste the
URL into the publisher and it will automatically be converted into an
inline link. You don’t need to use Markdown unless you want the link to
look fancy!

#### Images

Embedding an image from the web into your message is similar to
including a link. Click the image button above the publisher window, and
paste the URL of the image into the pop-up which appears. This will
enter the URL into your post, wrapped in Markdown image code. Type some
“alt text” (the text you want to appear in your message if the image
can’t be displayed) over the highlighted text in square brackets. You
can also add an optional title, which will be displayed as a tool-tip
when the cursor is moved over the image.

Note: For this to work, you must provide a direct link to a valid image
file (ending in .jpg, .gif, .png or similar), *not* to a web page with
an image or images on it.

diaspora\* uses a simple mark-up system called Markdown. If you want to
do anything not covered by the formatting buttons, you can find more
about how to use Markdown in [this tutorial](/formatting).

#### Embedding

You can also paste links to media sites such as YouTube, Vimeo,
Soundcloud and others, and the video/audio file you’re linking to will
become “embedded” in your post. Try it!

Note that the video/audio will only become embedded *after* you have
posted your message, so you’ll have to refresh/reload the stream in
order to see it. Occasionally it takes a while for the site which hosts
the file you want to embed to respond with the embed information, so you
might have to wait a short while and refresh the stream again.

#### Posting to other services

If you have conntected your diaspora\* seed to your accounts on other
services, clicking on one or more of the icons for those services below
the publisher means your message will appear on those networks as well.

![Connected services](images/partage-4.png)

When you start writing a message with one of more of these icons
highlighted, a character counter will show how many more characters are
available to you: 140 for Twitter, 1000 for Tumblr, 63,206 for Facebook.
In diaspora\*, you can post a whopping 65,535 characters! The counter
will always display the number of characters remaining for the service
selected which allows the fewest characters; so, if you highlight the
Twitter and Facebook logos, it will count down from 140 characters.

If your message is longer than the figure allowed by the service you’re
posting to, your message will appear truncated on that service, with a
link to your post in diaspora\*.

#### Post preview

Between the aspects and share buttons is the last button – and a really
useful one! This allows you to see what your post will look like, so you
can be sure you’re happy with it before you actually post it. This can
really help with correcting errors, and especially with text formatting.
More on this below.

That is all there is to know about the buttons and icons on the
publisher. The real magic happens within the publisher itself. As well
as plain text, you can add \#tags, @mentions, links, format your text
and embed photos, videos and audio. Wow!

![Publisher content](images/partage-5.png)

![Publisher preview](images/partage-6.png)

How about that? There’s plenty of things you can do in your posts in
diaspora\*.

To try it out, why not make a public announcement that you’ve just
arrived in diaspora\*? This is a good way to get welcomed by people in
our community, and to start to make connections.

To make a public post, just set the aspect selector button to Public,
and type your post. If you didn’t do it earlier, why not introduce
yourself to the diaspora\* community by including the \#newhere tag?
This way, people who follow that tag will see your post and be able to
welcome you. Include some of your interests as \#tags as well, and
you’ll find that people with similar interests to you will find you.
Your message might be something like:

> Hi everyone, I’m \#newhere in diaspora\*. I like \#running,
> \#literature and \#rabbits.

Of course, you might not want to make a public post. If not, that’s
fine: the important thing is that you know how to do it.

### Interacting with posts in the stream

The content stream is a real-time display of status messages. Whenever
someone you follow posts a status message, it gets added to the stream.
Naturally you can comment on your own posts and those made by other
people. You can also “like” and “reshare” other people’s messages and,
if someone posts something you really don’t want to see, you can hide
it. All of this happens in the content stream, which can be found just
below the publisher.

Don’t forget that there are different content streams depending on which
of the “views” you are using: Stream, My activity, @Mentions, My aspects
or \#Followed tags. While this alters which posts appear in your stream,
you can interact with those posts in the same way in any of these views.

To help diaspora\* pods perform faster, your stream won’t be constantly
reloaded automatically. Just refresh the page whenever you want to check
for updates to your stream.

#### Comments

If there are no comments on a post yet, click Comment to open a comment
field. When there are already comments below the post, the comment field
will automatically be there. If more than three comments have been
added, the last three comments will be shown. You can expand the full
thread by clicking

#### Likes

Next to the Comment link you will find a Like link. This can be used to
give the owner of the post a sign that you have read and appreciated
their post, without having to actually comment. When you click the link,
you will be added to a counter of the number of likes. Clicking this
counter will show you those people who have liked the post.

If you change your mind, you can remove your “like” by clicking the
Unlike link which is now visible. But be aware that the person whose
post you originally liked has already been sent a notification!

#### Reshare posts

Public posts made by other people can be reshared. In order to reshare a
post, click the Reshare link, which can be found between the Like and
Comment links. Resharing a post makes it visible to your followers, and
can help spread a good message to a new audience.

 

When you hover over any post in the stream, several icons will appear at
the top right-hand corner of the post. These allow you to do some useful
things:

#### Deleting posts and comments

When you hover your mouse pointer over one of your own posts or one of
the comments, an x will appear in the top right corner, which allows you
to delete the post or comment. You can also delete your own comments on
other people’s posts.

#### Hiding someone’s post

If you don’t want to see someone else’s post again, you can hide it from
your stream by clicking the x, just as you would to delete your own
post.

#### Enable/disable notifications

Next to the x is a little bell icon. If the bell is gray, that means you
aren’t currently receiving notifications of activity on this post. Click
it to turn the bell black, and you will now receive notifications for
this post. If you’re already receiving notifications and no longer want
them, click the bell to turn it from black to gray.

#### Ignore a user

If you find that a person’s posts are annoying, boring or offending you,
you can ignore that person. This means that their posts will no longer
appear in your stream; however, they will still be able to comment on,
like or reshare your posts, and their comments on posts which appear in
your stream will still be visible to you. To ignore someone, click the
ignore icon (a circle with a diagonal line through it). Their posts will
magically disappear from your screen.

A list of people you are ignoring can be found in your account settings
under Privacy. If you want to stop ignoring someone, you can remove them
from the list on that page. We cover how to change your account settings
page in [Part 7](7-finishing.html).

#### Report someone’s post

If you believe a post contravenes your pod’s code of conduct, for
instance if you believe it is abusive or is spam, you can click the
report icon (an exclamation point in a triangle). This will bring up a
report form, which will be sent to your pod’s admin. Please make sure
you’re aware of your pod’s **Terms of use** before reporting a post. You
can find a link to your pod’s Terms of use in the sidebar.

You’ve now learned about the activity you’ll be spending most of your
time in diaspora\* doing: posting status messages and commenting on and
sharing those made by others. In the next part of this tutorial we will
take a look at “conversations:” private messages shared with one or more
contacts.

[Part 4 – Finding and connecting with
people](4-connecting.html) |
[Part 6 – Notifications and
conversations](6-conversations.html)

#### Useful Resources

-   [Codebase](http://github.com/diaspora/diaspora/)
-   [Documentation](https://wiki.diasporafoundation.org/)
-   [Find & report bugs](http://github.com/diaspora/diaspora/issues)
-   [IRC - General](http://webchat.freenode.net/?channels=diaspora)
-   [IRC -
    Development](http://webchat.freenode.net/?channels=diaspora-dev)
-   [Discussion -
    General](http://groups.google.com/group/diaspora-discuss)
-   [Discussion -
    Development](http://groups.google.com/group/diaspora-dev)

[![Creative Commons
License](images/cc_by.png)](http://creativecommons.org/licenses/by/3.0/)
[diasporafoundation.org](https://diasporafoundation.org/) is licensed
under a [Creative Commons Attribution 3.0 Unported
License](http://creativecommons.org/licenses/by/3.0/)
