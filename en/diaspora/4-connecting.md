Getting started in diaspora\*
=============================

Part 4 – Finding and connecting with people
-------------------------------------------

It’s now time to add some contacts to the “Diaspora” aspect you just
created. If you already know people who are using diaspora\*, you can
find them and add them to any aspect you like.

### Sharing

We call connecting with someone “sharing” because it’s an indication
that you want to share content with them. Sharing in diaspora\* can seem
a bit confusing at first, because your levels of sharing with other
people are unlikely to be the same as their levels of sharing with you.
Let’s try to make sense of what that means.

On diaspora\* there are three types of relations between users:

#### Followers

Someone has placed you into one of their aspects, but you haven’t done
likewise for them. You will get a notification that this person has
“started sharing with you” but you won’t notice any other change.

They will see your public posts in their stream, but none of your
limited posts.

There’s no way to tell which aspect someone else has placed you in, for
privacy reasons – it’s their business which of their aspects they place
each person in, so no one else can find out.

#### Following

You are sharing with a person who is not sharing with you. This means
that you have added them to one (or more) of your aspects, but they have
not added you to their aspects.

The person you are following will have access to posts you have made to
the aspect(s) you have placed them in as well as your public posts, and
you will start receiving their public posts in your stream.

#### Mutual sharing

If two of you have started sharing with each other, things get more
interesting, and more complicated! This can be compared to being
“friends” on Facebook, although there are important differences.

When there’s a mutual connection, the two of you have each indicated a
desire to see posts from the other, so each of you will see limited
posts made by the other person to the aspect(s) into which the other has
placed you. However, your sharing with each other might be very
different. Think of the following scenario:

-   You consider Jill a slight acquaintance, and put her in your
    “Acquaintances” aspect.
-   Jill, however, thinks of you one of her best friends, and puts you
    in her “Close friends” aspect.
-   You post most things only to your “Friends” or “Family” aspects, so
    Jill doesn’t see many of your posts.
-   Because you’re in Jill’s “Close friends” aspect, on the other hand,
    you see almost everything she posts.

The main thing to remember is that no one will ever see a post unless
you have made it public or have made it to an aspect into which you have
manually placed that person.

### Finding people

In order to start sharing with people, you will have to start following
some people. Perhaps they will follow you back! Let’s look at how to do
that now.

There are several ways to find someone and add them to an aspect.

#### Search

The search field is in the header bar. To add someone by search:

-   Type a name or diaspora\* ID. Suggestions will appear as you start
    typing.
-   Hit enter to search, and you will land on a page with results.
-   When you have found the person you are looking for, click the Add
    contact button.
-   If the person you want to start sharing with appears in the list of
    suggestions, just select their name to get to their profile page and
    click the Add contact button from there.
-   A drop-down menu will then appear under the button in which you can
    select which aspect(s) to add the person to. You can also create a
    new aspect and add them to that aspect by clicking *+ Add an
    aspect*.

![Add contact](images/echange-1.png)

#### Sending an invitation mail

Another way to follow a friend is by sending them an invitation mail.
This can be done through the by mail link in the menu on the right of
the search results page or from the Invite your friends link in the
sidebar of the stream page. As soon as your friend accepts the invite,
they will be taken through the same registration process you have just
been through.

![Invite](images/echange-2.png)

#### From their profile

Another way to add a person to one of your aspects is by clicking their
name wherever you see it in diaspora\*. This will take you to their
profile page, from where you will be able to add them to aspects using
the button in the top right corner of the page.

Alternatively, just hover over their name or profile photo in the
stream, and a small “hover-card” will appear. You can add them to an
aspect directly from this hover-card.

#### Following \#tags

The above three options focus on connecting to people you already know.
But sometimes strangers can be just as interesting, if not more
interesting, than people you already know. A great way to start
connecting with people is to start following some \#tags of subjects
that interest you. Posts containing those tags will then appear in your
stream, and you can follow people who post things which you find
interesting, by placing them into one of your aspects.

### Removing someone from your contacts

To remove someone from your contacts completely and thereby stop sharing
with them, all you need to do is to remove them from all aspects you
have placed them in. You can do this by:

1.  either going to their profile page or hovering your cursor over
    their name in the stream and waiting for a hovercard to appear;
2.  clicking the arrow on the green aspect selector button and deselect
    all the aspects ticked.

That’s it! Once they are no longer in any of your aspects, you are no
longer sharing with that person.

You can also remove a person from your aspects via your contacts page,
which we covered at the end of [Part 3](3-aspects.html).

You now know how to start sharing with people, and hopefully you’ve been
able to add some contacts. You’ll now start seeing content from them in
your stream; and once people have started sharing with you, you’ll have
an audience for your posts. **It’s time to start publishing some
content.**

[Part 3 –
Aspects](3-aspects.html) | [Part
5 – Start
sharing!](5-sharing.html)

#### Useful Resources

-   [Codebase](http://github.com/diaspora/diaspora/)
-   [Documentation](https://wiki.diasporafoundation.org/)
-   [Find & report bugs](http://github.com/diaspora/diaspora/issues)
-   [IRC - General](http://webchat.freenode.net/?channels=diaspora)
-   [IRC -
    Development](http://webchat.freenode.net/?channels=diaspora-dev)
-   [Discussion -
    General](http://groups.google.com/group/diaspora-discuss)
-   [Discussion -
    Development](http://groups.google.com/group/diaspora-dev)

[![Creative Commons
License](images/cc_by.png)](http://creativecommons.org/licenses/by/3.0/)
[diasporafoundation.org](https://diasporafoundation.org/) is licensed
under a [Creative Commons Attribution 3.0 Unported
License](http://creativecommons.org/licenses/by/3.0/)
